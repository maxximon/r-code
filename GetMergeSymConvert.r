library(quantmod)
s <- c("MSFT","C","MMM")
e <- new.env() #environment in which to store data
getSymbols(s, src="yahoo", env=e)
do.call(merge, eapply(e, Cl)[s])

Or, using try like the OP

L <- lapply(symbols, function(sym) try(getSymbols(sym, auto.assign=FALSE)))
do.call(merge, lapply(L, Cl))
####################################################################
tickers <- c("SPY","DIA","IWM","SMH","OIH","XLY",
             "XLP","XLE","XLI","XLB","XLK","XLU")
getSymbols(tickers, from="2001-03-01", to="2011-03-11")
ClosePrices <- do.call(merge, lapply(tickers, function(x) Cl(get(x))))
head(ClosePrices)

down vote


Try using the env= arg and eapply

> mystocks <- new.env(hash=TRUE)

> getSymbols(c("AAPL","GOOG","YHOO"), env=mystocks)
<environment: 0x1023d1240>
  
  > head( do.call(cbind,eapply(mystocks, Cl)) )

2 down vote accepted


Your code almost works if you just convert to xts. If you're having difficulty converting your data.frame to xts, then provide more info about your data as requested in the comments of your question.

getSymbols("SPY", src='yahoo', return.class='data.frame')
#[1] "SPY"
class(SPY)
#[1] "data.frame"
as.data.frame(periodReturn(xts(SPY[["SPY.Close"]], as.Date(rownames(SPY))), 
'yearly', subset="2008/"))
yearly.returns
2008-12-31   -0.382805554
2009-12-31    0.234929078
2010-12-31    0.128409907
2011-12-30   -0.001988072
