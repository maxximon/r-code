
#Vanguard-Sector
#sectorVanguard
asset.names <- c("VCR" ,  #Consumer Discretionary
"VDC",  # Consumer Staples
"VDE",  #Energy
"VFH", # Financials
"VHT", # Health Care
"VIS",  # Industrials
"VGT", # Information 
"VAW",  # Materials
"VNQ",  # REIT
"VOX",  #Telecommunications
"VNQI", # Global ex-U.S. Real Estate ETF (VNQI).
"VPU") # Utilities
"WM", #iShares Russell 2000 ETF (small cap US equities)
"IVV",# iShares S&P 500 large cap US equities)
"EFA", #iShares MSCI EAFE Index (international equities)
"ICF", #iShares Cohen & Steers Realty Majors (real estate)
"DBC", #PowerShares DB Commodity (commodities)
"VWO", #Vanguard FTSE Emerging Markets (emerging markets)
"IAU", #iShares Gold Trust (gold)
"TLT", #iShares 20+ Treasury (long US bonds)
"SHY", #iShares 1-3 Year Treasury (short bonds)

vdc.prices = get.hist.quote(instrument="vdc", start="2005-09-01",
                             end="2010-9-30", quote="AdjClose",
                             provider="yahoo", origin="1970-01-01",
                             compression="m", retclass="zoo")
vde.prices = get.hist.quote(instrument="vde", start="2005-09-01",
                             end="2010-9-30", quote="AdjClose",
                             provider="yahoo", origin="1970-01-01",
                             compression="m", retclass="zoo")
vfh.prices = get.hist.quote(instrument="vfh", start="2005-09-01",
                             end="2010-9-30", quote="AdjClose",
                             provider="yahoo", origin="1970-01-01",
                             compression="m", retclass="zoo")
vht.prices = get.hist.quote(instrument="vht", start="2005-09-01",
                             end="2010-9-30", quote="AdjClose",
                             provider="yahoo", origin="1970-01-01",
                             compression="m", retclass="zoo")
vis.prices = get.hist.quote(instrument="vis", start="2005-09-01",
                             end="2010-9-30", quote="AdjClose",
                             provider="yahoo", origin="1970-01-01",
                             compression="m", retclass="zoo")
vgt.prices = get.hist.quote(instrument="vgt", start="2005-09-01",
                             end="2010-9-30", quote="AdjClose",
                             provider="yahoo", origin="1970-01-01",
                             compression="m", retclass="zoo")
iau.prices = get.hist.quote(instrument="vfh", start="2005-09-01",
                             end="2010-9-30", quote="AdjClose",
                             provider="yahoo", origin="1970-01-01",
                             compression="m", retclass="zoo")
vaw.prices = get.hist.quote(instrument="vaw", start="2005-09-01",
                             end="2010-9-30", quote="AdjClose",
                             provider="yahoo", origin="1970-01-01",
                             compression="m", retclass="zoo")
vnq.prices = get.hist.quote(instrument="vnq", start="2005-09-01",
                             end="2010-9-30", quote="AdjClose",
                             provider="yahoo", origin="1970-01-01",
                             compression="m", retclass="zoo")
vpu.prices = get.hist.quote(instrument="vpu", start="2005-09-01",
                             end="2010-9-30", quote="AdjClose",
                             provider="yahoo", origin="1970-01-01",
                             compression="m", retclass="zoo")
wm.prices = get.hist.quote(instrument="vht", start="2005-09-01",
                             end="2010-9-30", quote="AdjClose",
                             provider="yahoo", origin="1970-01-01",
                             compression="m", retclass="zoo")
gld.prices = get.hist.quote(instrument="gld", start="2005-09-01",
                             end="2010-9-30", quote="AdjClose",
                             provider="yahoo", origin="1970-01-01",
                             compression="m", retclass="zoo")
vox.prices = get.hist.quote(instrument="vgt", start="2005-09-01",
                             end="2010-9-30", quote="AdjClose",
                             provider="yahoo", origin="1970-01-01",
                             compression="m", retclass="zoo")
vpu.prices = get.hist.quote(instrument="vpu", start="2005-09-01",
                             end="2010-9-30", quote="AdjClose",
                             provider="yahoo", origin="1970-01-01",
                             compression="m", retclass="zoo")
vaw.prices = get.hist.quote(instrument="vht", start="2005-09-01",
                             end="2010-9-30", quote="AdjClose",
                             provider="yahoo", origin="1970-01-01",
                             compression="m", retclass="zoo")
iyy.prices = get.hist.quote(instrument="iyy", start="2005-09-01",
                             end="2010-9-30", quote="AdjClose",
                             provider="yahoo", origin="1970-01-01",
                             compression="m", retclass="zoo")
gspc.prices = get.hist.quote(instrument="^gspc", start="2005-09-01",
                             end="2010-9-30", quote="AdjClose",
                             provider="yahoo", origin="1970-01-01",
                             compression="m", retclass="zoo")
# change time indices to class yearmon, which is most appropriate for monthly data
index(vfinx.prices) = as.yearmon(index(vfinx.prices))
index(veurx.prices) = as.yearmon(index(veurx.prices))
index(veiex.prices) = as.yearmon(index(veiex.prices))
index(vbltx.prices) = as.yearmon(index(vbltx.prices))
index(vbisx.prices) = as.yearmon(index(vbisx.prices))
index(vpacx.prices) = as.yearmon(index(vpacx.prices))


###########################################################
projectPrices.z = merge(vfinx.prices,veurx.prices,veiex.prices,vbltx.prices,
                        vbisx.prices,vpacx.prices)
colnames(projectPrices.z) = asset.names
# create data.frame for downloading
projectPrices.df = coredata(projectPrices.z)
rownames(projectPrices.df) = as.character(index(projectPrices.z))

#
# compute cc and simple returns
#

projectReturns.z = diff(log(projectPrices.z))   
projectReturnsSimple.z = exp(projectReturns.z) - 1
# create data.frame for downloading
projectReturns.df = as.data.frame(coredata(projectReturns.z))
rownames(projectReturns.df) = as.character(index(projectReturns.z))
projectReturnsSimple.df = as.data.frame(coredata(projectReturnsSimple.z))
rownames(projectReturnsSimple.df) = as.character(index(projectReturnsSimple.z))
