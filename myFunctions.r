"  these are my functions ####################
###################################################
#enter ret vector and starting bal
growInvestment <- function (returns, startbalance){
 # Pass this function a numeric vector of returns in the format (.2, -.4, etc.)
 # Returns matrix of cumulative returns with first row as starting balance
 n <- length(returns)
 r <- matrix(nrow=n+1)
 for (i in 1:(n+1)) {
 if(i==1){
 r[i,] <- startbalance
 } else {
 r[i,] <- (1+returns[i-1]) * r[i-1,]
 }
 }
 rownames(r) <- rownames(returns)
 return(r)
}
###################################################################
gm_mean = function(x, na.rm=TRUE){
  exp(sum(log(x[x > 0]), na.rm=na.rm) / length(x))
}
#gm <-c(2,4,6,8,10)
#gm_mean(gm)

###################################################
